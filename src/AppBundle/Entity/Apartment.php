<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Apartment
 *
 * @ORM\Table(name="apartment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApartmentRepository")
 */
class Apartment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="housenumber", type="string", length=255)
     */
    private $housenumber;

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=255)
     */
    private $domain;

    /**
     * @var string
     *
     * @ORM\Column(name="housetype", type="string", length=255)
     */
    private $housetype;

    /**
     * @var string
     *
     * @ORM\Column(name="typecode", type="string", length=255)
     */
    private $typecode;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="string", length=255)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="deliveryDate", type="string", length=255)
     */
    private $deliveryDate;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="ad_text", type="text")
     */
    private $adText;

    /**
     * @var string
     *
     * @ORM\Column(name="youtube_link", type="string")
     */
    private $youtubeLink;

    /**
     * @var int
     *
     * @ORM\Column(name="total_apartment", type="integer")
     */
    private $totalApartment;

    /**
     * @var int
     *
     * @ORM\Column(name="total_living_room", type="integer")
     */
    private $totalLivingRoom;

    /**
     * @var int
     *
     * @ORM\Column(name="total_kitchen", type="integer")
     */
    private $totalKitchen;

    /**
     * @var int
     *
     * @ORM\Column(name="total_bedroom1", type="integer")
     */
    private $totalBedroom1;

    /**
     * @var int
     *
     * @ORM\Column(name="total_bedroom2", type="integer")
     */
    private $totalBedroom2;

    /**
     * @var int
     *
     * @ORM\Column(name="total_bedroom3", type="integer")
     */
    private $totalBedroom3;

    /**
     * @var int
     *
     * @ORM\Column(name="total_bedroom4", type="integer")
     */
    private $totalBedroom4;

    /**
     * @var int
     *
     * @ORM\Column(name="total_bathroom", type="integer")
     */
    private $totalBathroom;

    /**
     * @var int
     *
     * @ORM\Column(name="total_garden", type="integer")
     */
    private $totalGarden;

    /**
     * @var int
     *
     * @ORM\Column(name="total_roof", type="integer")
     */
    private $totalRoof;

    /**
     * @var string
     *
     * @ORM\Column(name="erfpacht_canon", type="string", length=255)
     */
    private $erfpachtCanon;

    /**
     * @var string
     *
     * @ORM\Column(name="waarborg_certificaat", type="string", length=255)
     */
    private $waarborgCertificaat;

    /**
     * @var string
     *
     * @ORM\Column(name="seller", type="string", length=255)
     */
    private $seller;

    /**
     * @var string
     *
     * @ORM\Column(name="door_type", type="string", length=255)
     */
    private $doorType;

    /**
     * @var string
     *
     * @ORM\Column(name="door_link", type="string", length=255)
     */
    private $doorLink;

    /**
     * @var string
     *
     * @ORM\Column(name="ventilation_type", type="string", length=255)
     */
    private $ventilationType;

    /**
     * @var string
     *
     * @ORM\Column(name="entrance_security", type="string", length=255)
     */
    private $entranceSecurity;

    /**
     * @var string
     *
     * @ORM\Column(name="stuc_werk", type="string", length=255)
     */
    private $stucWerk;

    /**
     * @var string
     *
     * @ORM\Column(name="entrance_lock", type="string", length=255)
     */
    private $entranceLock;

    /**
     * @var string
     *
     * @ORM\Column(name="stairs_protection", type="string", length=255)
     */
    private $stairsProtection;

    /**
     * @var string
     *
     * @ORM\Column(name="total_extra_costs", type="string", length=255)
     */
    private $totalExtraCosts;

    /**
     * @var string
     *
     * @ORM\Column(name="hot_water_for_machine", type="string", length=255)
     */
    private $hotWaterForMachine;

    /**
     * @var int
     *
     * @ORM\Column(name="analytics", type="integer")
     */
    private $analytics;

    /**
     * @var string
     *
     * @ORM\Column(name="display_overview_website", type="string", length=255)
     */
    private $displayOverviewWebsite;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Apartment
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set housenumber
     *
     * @param string $housenumber
     *
     * @return Apartment
     */
    public function setHousenumber($housenumber)
    {
        $this->housenumber = $housenumber;

        return $this;
    }

    /**
     * Get housenumber
     *
     * @return string
     */
    public function getHousenumber()
    {
        return $this->housenumber;
    }

    /**
     * Set domain
     *
     * @param string $domain
     *
     * @return Apartment
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set housetype
     *
     * @param string $housetype
     *
     * @return Apartment
     */
    public function setHousetype($housetype)
    {
        $this->housetype = $housetype;

        return $this;
    }

    /**
     * Get housetype
     *
     * @return string
     */
    public function getHousetype()
    {
        return $this->housetype;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Apartment
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set deliveryDate
     *
     * @param string $deliveryDate
     *
     * @return Apartment
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;

        return $this;
    }

    /**
     * Get deliveryDate
     *
     * @return string
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Apartment
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set typecode
     *
     * @param string $typecode
     *
     * @return Apartment
     */
    public function setTypecode($typecode)
    {
        $this->typecode = $typecode;

        return $this;
    }

    /**
     * Get typecode
     *
     * @return string
     */
    public function getTypecode()
    {
        return $this->typecode;
    }

    /**
     * @return string
     */
    public function getAdText()
    {
        return $this->adText;
    }

    /**
     * @param string $adText
     */
    public function setAdText($adText)
    {
        $this->adText = $adText;
    }

    /**
     * @return string
     */
    public function getYoutubeLink()
    {
        return $this->youtubeLink;
    }

    /**
     * @param string $youtubeLink
     */
    public function setYoutubeLink($youtubeLink)
    {
        $this->youtubeLink = $youtubeLink;
    }

    /**
     * @return int
     */
    public function getTotalApartment()
    {
        return $this->totalApartment;
    }

    /**
     * @param int $totalApartment
     */
    public function setTotalApartment($totalApartment)
    {
        $this->totalApartment = $totalApartment;
    }

    /**
     * @return int
     */
    public function getTotalLivingRoom()
    {
        return $this->totalLivingRoom;
    }

    /**
     * @param int $totalLivingRoom
     */
    public function setTotalLivingRoom($totalLivingRoom)
    {
        $this->totalLivingRoom = $totalLivingRoom;
    }

    /**
     * @return int
     */
    public function getTotalKitchen()
    {
        return $this->totalKitchen;
    }

    /**
     * @param int $totalKitchen
     */
    public function setTotalKitchen($totalKitchen)
    {
        $this->totalKitchen = $totalKitchen;
    }

    /**
     * @return int
     */
    public function getTotalBedroom1()
    {
        return $this->totalBedroom1;
    }

    /**
     * @param int $totalBedroom1
     */
    public function setTotalBedroom1($totalBedroom1)
    {
        $this->totalBedroom1 = $totalBedroom1;
    }

    /**
     * @return int
     */
    public function getTotalBedroom2()
    {
        return $this->totalBedroom2;
    }

    /**
     * @param int $totalBedroom2
     */
    public function setTotalBedroom2($totalBedroom2)
    {
        $this->totalBedroom2 = $totalBedroom2;
    }

    /**
     * @return int
     */
    public function getTotalBedroom3()
    {
        return $this->totalBedroom3;
    }

    /**
     * @param int $totalBedroom3
     */
    public function setTotalBedroom3($totalBedroom3)
    {
        $this->totalBedroom3 = $totalBedroom3;
    }

    /**
     * @return int
     */
    public function getTotalBathroom()
    {
        return $this->totalBathroom;
    }

    /**
     * @param int $totalBathroom
     */
    public function setTotalBathroom($totalBathroom)
    {
        $this->totalBathroom = $totalBathroom;
    }

    /**
     * @return int
     */
    public function getTotalGarden()
    {
        return $this->totalGarden;
    }

    /**
     * @param int $totalGarden
     */
    public function setTotalGarden($totalGarden)
    {
        $this->totalGarden = $totalGarden;
    }

    /**
     * @return int
     */
    public function getTotalRoof()
    {
        return $this->totalRoof;
    }

    /**
     * @param int $totalRoof
     */
    public function setTotalRoof($totalRoof)
    {
        $this->totalRoof = $totalRoof;
    }

    /**
     * @return string
     */
    public function getErfpachtCanon()
    {
        return $this->erfpachtCanon;
    }

    /**
     * @param string $erfpachtCanon
     */
    public function setErfpachtCanon($erfpachtCanon)
    {
        $this->erfpachtCanon = $erfpachtCanon;
    }

    /**
     * @return int
     */
    public function getTotalBedroom4()
    {
        return $this->totalBedroom4;
    }

    /**
     * @param int $totalBedroom4
     */
    public function setTotalBedroom4($totalBedroom4)
    {
        $this->totalBedroom4 = $totalBedroom4;
    }

    /**
     * @return string
     */
    public function getWaarborgCertificaat()
    {
        return $this->waarborgCertificaat;
    }

    /**
     * @param string $waarborgCertificaat
     */
    public function setWaarborgCertificaat($waarborgCertificaat)
    {
        $this->waarborgCertificaat = $waarborgCertificaat;
    }

    /**
     * @param string $seller
     */
    public function setSeller($seller)
    {
        $this->seller = $seller;
    }

    /**
     * @return string
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * @return int
     */
    public function getAnalytics()
    {
        return $this->analytics;
    }

    /**
     * @param int $analytics
     */
    public function setAnalytics($analytics)
    {
        $this->analytics = $analytics;
    }

    /**
     * @return string
     */
    public function getDoorType()
    {
        return $this->doorType;
    }

    /**
     * @param string $doorType
     */
    public function setDoorType($doorType)
    {
        $this->doorType = $doorType;
    }

    /**
     * @return string
     */
    public function getVentilationType()
    {
        return $this->ventilationType;
    }

    /**
     * @param string $ventilationType
     */
    public function setVentilationType($ventilationType)
    {
        $this->ventilationType = $ventilationType;
    }

    /**
     * @return string
     */
    public function getEntranceSecurity()
    {
        return $this->entranceSecurity;
    }

    /**
     * @param string $entranceSecurity
     */
    public function setEntranceSecurity($entranceSecurity)
    {
        $this->entranceSecurity = $entranceSecurity;
    }

    /**
     * @return string
     */
    public function getEntranceLock()
    {
        return $this->entranceLock;
    }

    /**
     * @param string $entranceLock
     */
    public function setEntranceLock($entranceLock)
    {
        $this->entranceLock = $entranceLock;
    }

    /**
     * @return string
     */
    public function getStairsProtection()
    {
        return $this->stairsProtection;
    }

    /**
     * @param string $stairsProtection
     */
    public function setStairsProtection($stairsProtection)
    {
        $this->stairsProtection = $stairsProtection;
    }

    /**
     * @return string
     */
    public function getTotalExtraCosts()
    {
        return $this->totalExtraCosts;
    }

    /**
     * @param string $totalExtraCosts
     */
    public function setTotalExtraCosts($totalExtraCosts)
    {
        $this->totalExtraCosts = $totalExtraCosts;
    }

    /**
     * @return string
     */
    public function getHotWaterForMachine()
    {
        return $this->hotWaterForMachine;
    }

    /**
     * @param string $hotWaterForMachine
     */
    public function setHotWaterForMachine($hotWaterForMachine)
    {
        $this->hotWaterForMachine = $hotWaterForMachine;
    }

    /**
     * @return string
     */
    public function getDoorLink()
    {
        return $this->doorLink;
    }

    /**
     * @param string $doorLink
     */
    public function setDoorLink($doorLink)
    {
        $this->doorLink = $doorLink;
    }

    /**
     * @return string
     */
    public function getDisplayOverviewWebsite()
    {
        return $this->displayOverviewWebsite;
    }

    /**
     * @param string $displayOverviewWebsite
     */
    public function setDisplayOverviewWebsite($displayOverviewWebsite)
    {
        $this->displayOverviewWebsite = $displayOverviewWebsite;
    }

    /**
     * @return string
     */
    public function getStucWerk()
    {
        return $this->stucWerk;
    }

    /**
     * @param string $stucWerk
     */
    public function setStucWerk($stucWerk)
    {
        $this->stucWerk = $stucWerk;
    }


}
