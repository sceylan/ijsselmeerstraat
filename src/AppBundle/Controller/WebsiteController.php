<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Apartment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class WebsiteController extends Controller
{

    /**
     * Override method to call #containerInitialized method when container set.
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->containerInitialized();
    }

    /**
     * Perform some operations after controller initialized and container set.
     */
    private function containerInitialized()
    {
        $domain = $_SERVER['SERVER_NAME'];
        $domainStripped = str_replace('www.', '', $domain);

        $apartment = $this->getDoctrine()->getRepository('AppBundle:Apartment')->findOneBy(array('domain' => $domainStripped));
        $this->container->get('twig')->addGlobal('apartment', $apartment);

        if(!$apartment)
            throw $this->createNotFoundException('Pagina niet gevonden');

    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $apartments = $this->getDoctrine()->getRepository('AppBundle:Apartment')->findBy(['displayOverviewWebsite' => 'ja']);
        return $this->render("@App/Website/index.html.twig", ['apartments' => $apartments]);
    }

    /**
     * @Route("/het-project/", name="project")
     */
    public function projectAction(Request $request)
    {
        return $this->render("@App/Website/project.html.twig");
    }

    /**
     * @Route("/de-woning/", name="woning")
     */
    public function woningAction(Request $request)
    {
        return $this->render("@App/Website/woning.html.twig");
    }

    /**
     * @Route("/contact/", name="contact")
     * @Template()
     */
    public function contactAction(Request $request, \Swift_Mailer $mailer)
    {

        if($request->isMethod('POST'))
        {
            $firstname = $request->get('firstname');
            $lastname = $request->get('lastname');
            $email = $request->get('email');
            $phone = $request->get('phone');
            $message = $request->get('message');

            $mandatoryInputValues = array($firstname, $lastname, $email, $phone);
            $error = false;

            foreach($mandatoryInputValues as $input) {
                if(strlen($input) < 2) {
                    $error = true;
                }
            }

            if($error === true) {
                $this->addFlash('error', 'Een fout is opgetreden. Controleer het formulier en probeer het opnieuw.');
                return $this->render("@App/Website/contact.html.twig");
            }

            $twig = $this->container->get('twig');
            $globals = $twig->getGlobals();

            /**
             * @var $apartment Apartment
             */
            $apartment = $globals['apartment'];

            if($error === false)
            {
                $message = (new \Swift_Message())
                    ->setSubject("Nieuwe contactverzoek aanvraag")
                    ->setFrom(['info@ijsselmeerstraat.nl' => 'Ijsselmeerstraat.nl'])
                    ->setTo(['projecten@vandesteege.nl' => 'ERA Van De Steege'])
                    ->setBcc(['aanvragen@ijsselmeerstraat.nl' => 'Ijsselmeerstraat.nl'])
                    ->setBody(
                        $this->renderView(
                            '@App/Website/mail.html.twig',
                            [
                                'firstname' => $firstname,
                                'lastname' => $lastname,
                                'email' => $email,
                                'phone' => $phone,
                                'message' => $message,
                                'apartment' => $apartment
                            ]
                        ),
                        'text/html'
                    );

                $mailer->send($message);
                $this->addFlash('success', 'Uw contact verzoek is succesvol verstuurd. Wij zullen zo spoedig mogelijk contact met u opnemen.');
                return $this->redirectToRoute('contact');
            }

        }

        return $this->render("@App/Website/contact.html.twig");

    }


}
